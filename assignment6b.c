#include <stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

void input_array(int n,int a[n])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the element no %d of the array\n",i);
        scanf("%d",&a[i]);
    }
}
void print(int n, int a[n])
{
    printf("The elements of the array are:\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\n",a[i]);
    }
}
int main()
{
    int n;
    n=input();
    int a[n];
    input_array(n,a);
    print(n,a);
    return 0;
}