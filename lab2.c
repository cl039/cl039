#include<stdio.h>
float input()
{
	float a;
	printf("enter the value\n");
	scanf("%f",&a);
	return a;
}
float compute(float a)
{
	float area;
	area=3.14*a*a;
	return area;
}
void output(float a, float area)
{
	printf("the area of the circle with radius %f is %f\n",a,area);
}
int main()
{
	float x,y;
	x=input();
	y=compute(x);
	output(x,y);
	return 0;
}
