#include<stdio.h>
#include<string.h>
struct student{
    int rn;
    char name[100];
    char section[100];
    char dept[100];
    float fees;
    int marks;
};
typedef struct student stud;
int input()
{
    int n;
    printf("enter the number of students\n");
    scanf("%d",&n);
    return n;
}
void input_data(int n, stud e[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the info of stud %d\n", i+1);
        printf("enter the roll no.\n");
        scanf("%d",&e[i].rn);
        printf("enter the student name\n");
        gets(e[i].name);
        printf("enter the section\n");
        gets(e[i].section);
        printf("enter the department\n");
        gets(e[i].dept);
        printf("enter the fees\n");
        scanf("%f",&e[i].fees);
        printf("enter the marks scored\n");
        scanf("%d",&e[i].marks);
    }
}

void display(int n, stud e[n])
{
    for(int i=0;i<n;i++)
    {    
        printf("the info of stud %d\n",i+1);
        printf("the roll no.is %d\n",e[i].rn);
        printf("the student name is %s\n", e[i].name);
        printf("the section is %s\n", e[i].section);
        printf("the department is %s\n", e[i].dept);
        printf("the fees is %f\n", e[i].fees);
        printf("the marks scored are %d\n", e[i].marks);
        }
       
}
int main()
{
    int n,p;
    n=input();
    stud e[n];
    input_data(n,e);
    display(n,e);
    return 0;
}