#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c;
    float D,deno,root1,root2;
    printf("Enter the values of a,b and c");
    scanf("%d %d %d", &a,&b,&c);
    D=(b*b)-(4*a*c);
    deno=2*a;
    if(D>0)
    {
        printf("\nReal Roots");
        root1=(-b+sqrt(D))/deno;
        root2=(-b-sqrt(D))/deno;
        printf("Root1= %f \t Root2= %f", root1,root2);
    }
    else if(D==0)
    {
        printf("Equal roots");
        root1= -b/deno;
        printf("\n Root1= %f", root1);
    }
    else
	{
    	printf("Imaginary roots");
	}
    return 0;
}
