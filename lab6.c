#include<stdio.h>
int input()
{
    int n;
    printf("enter the no, of rows and columns\n");
    scanf("%d",&n);
    return n;
}
void input_array(int a, int b, int mat[a][b])
{
    printf("enter the elements of the array\n");
    for(int i=0;i<a;i++)
    {
        for(int j=0;j<b;j++)
        {
            scanf("%d",&mat[i][j]);
        }
    }
}
void transpose(int a, int b, int mat[a][b],int trans[b][a])
{
    for(int i=0;i<b;i++)
    {
        for(int j=0;j<a;j++)
        {
            trans[i][j]=mat[j][i];
        }
    }
}
void display(int a, int b,int  mat[a][b],int trans[b][a])
{
    printf("\n The elements of the matrix are ");
    for(int i=0;i<a;i++)
    {
        printf("\n");
        for(int j=0;j<b;j++)
        {
            printf("\t %d", mat[i][j]);
        }
    }
    printf("\n The elements of the transposed matrix are ");
    for(int i=0;i<b;i++)
    {
        printf("\n");
        for(int j=0;j<a;j++)
        {
            printf("\t %d",trans[i][j]);
        }
    }
}
int main()
{
    int p,q;
    p=input();
    q=input();
    int mat[p][q];
    input_array(p,q,mat);
    int trans[q][p];
    transpose(p,q,mat,trans);
    display(p,q,mat,trans);
    return 0;
}