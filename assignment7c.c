#include<stdio.h>
int input()
{
    int n;
    printf("enter the value of n\n");
    scanf("%d",&n);
    return n;
}
void input_array(int n, int a[n])
{
    printf("enter the numbers\n");
    for (int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}
int find(int n, int a[n])
{
    int  flag=0;
    for(int i=0;i<n;i++)
    {
        for(int j=i+1;j<n;j++)
        {
            if(a[i]==a[j])
            {
                flag+=1;
            }
        }
    }
    return flag;
}
void display(int n, int a[n], int flag)
{
    printf("the no. of duplicate elements are %d\n",flag);
}
int main()
{
    int n,f;
    n=input();
    int a[n];
    input_array(n,a);
    f=find(n,a);
    display(n,a,f);
    return 0;
}