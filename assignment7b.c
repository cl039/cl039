#include<stdio.h>
int input()
{
    int n;
    printf("enter the value of n\n");
    scanf("%d",&n);
    return n;
}
void input_array(int n, int a[n])
{
    printf("enter the numbers\n");
    for (int i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}
float sum(int n, int a[n])
{
    float sum=0;
    for(int i=0;i<n;i++)
    {
        sum+=a[i];
    }
    return sum;
}
float average(int n,int a[n], float sum)
{
    float avg;
    avg=sum/n;
    return avg;
}
void output( float sum ,float avg)
{
    printf("the sum is %f\nthe average is %.2f",sum, avg);
}
int main()
{
    int x;
    float y,z;
    x=input();
    int a[x];
    input_array(x,a);
    y=sum(x,a);
    z=average(x,a,y);
    output(y,z);
    return 0;
}