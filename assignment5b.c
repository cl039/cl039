#include<stdio.h>
float  input()
{
    float c;
    printf("enter the number of values\n");
    scanf("%f", &c);
    return c;
}
float sum(float c)
{
    float a, sum=0.0;
    int i=0;
    printf("enter the numbers\n");
    while(i<c)
    {
        scanf("%f", &a);
        sum+=a;
        i++;
    }
    return sum;
}
float average(float c, float sum)
{
    float average;
    average=sum/c;
    return average;
}
void display(float c, float sum, float average)
{
    printf("the sum is %.2f\n",sum);
    printf("the average is %.2f",average);
}
int main()
{
    float c;
    c=input();
    float p, q;
    p=sum(c);
    q=average(c,p);
    display(c,p,q);
    return 0;
}
