#include<stdio.h>
struct date_arrival
{
    int dd;
    int mm;
    int yy;
};
struct book_details
{
    char name[100];
    char author[100];
    struct date_arrival date;
    float price;
};
typedef struct book_details book;
int input()
{
    int n;
    printf("enter the number of books\n");
    scanf("%d",&n);
    return n;
}
void input_data(int n, book e[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the info of book %d\n", i+1);
        printf("enter the book name\n");
        gets(e[i].name);
        printf("enter the authors name\n");
        gets(e[i].author);
        printf("enter the date\n");
        scanf("%d%d%d",&e[i].date.dd, &e[i].date.mm, &e[i].date.yy);
        printf("enter the price\n");
        scanf("%f",&e[i].price);
    }
}

void display(int n, book e[n])
{
    for(int i=0;i<n;i++)
    {    
        printf("The info of book %d\n", i+1);
        printf("the book is ");
        puts(e[i].name);
        printf("the authors name is ");
        puts(e[i].author);
        printf("the date of arrival is %d-%d-%d\n",e[i].date.dd,e[i].date.mm,e[i].date.yy);
        printf("the price is %f\n", e[i].price);
    }
}
int main()
{
    int n,p;
    n=input();
    book e[n];
    input_data(n,e);
    display(n,e);
    return 0;
}