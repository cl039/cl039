#include<stdio.h>
int input()
{
    int n;
    printf("enter the size of array \n");
    scanf("%d",&n);
    return n;
}
void input_array(int n, int a[n])
{
    printf("enter the elements of the array\n");
    for(int i=0;i<n;i++)
    {
            scanf("%d",&a[i]);
    }
}
void search(int n, int a[n], int pos[n])
{
    int j=0, flag;
    printf("enter the number to be searched\n");
    scanf("%d",&flag);
    for(int i=0;i<n;i++)
    {
        if(a[i]==flag)
        {
            pos[j]=i+1;
            j=j+1;
        }
    }
    for(int i=0;i<n;i++)
    {
        if(a[i]!=flag)
        {
            pos[j]=0;
            j=j+1;
        }
    }
}
void display(int n,int a[n], int pos[n])
{
    printf("the element is in positions:\n");
    for(int i=0;i<n;i++)
    {
        printf("%d\t",pos[i]);
    }
}
int main()
{
    int p;
    p=input();
    int a[p];
    input_array(p,a);
    int pos[p];
    search(p,a,pos);
    display(p,a,pos);
    return 0;
}