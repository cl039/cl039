#include<stdio.h>
#include<string.h>
struct employee{
    int id;
    char name[100];
    char address[100];
    float salary;
};
typedef struct employee emp;
int input()
{
    int n;
    printf("enter the number of employees\n");
    scanf("%d",&n);
    return n;
}
void input_data(int n, emp e[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the info of emp %d\n", i+1);
        printf("enter the id\n");
        scanf("%d",&e[i].id);
        printf("enter the employee name\n");
        gets(e[i].name);
        printf("enter the address of the employee\n");
        gets(e[i].address);
        printf("enter the salary\n");
        scanf("%f",&e[i].salary);
    }
}
void display(int n, emp e[n])
{
    for(int i=0;i<n;i++)
    {
        printf("the info of emp %d\n", i+1);
        printf("the id is %d\n",e[i].id);
        printf("the employee name is %s", e[i].name);
        printf("\nthe address of the employee is %s\n",e[i].address);
        printf("the salary is %f\n",e[i].salary);
    }
}
int main()
{
    int n,p;
    n=input();
    emp e[n];
    input_data(n,e);
    display(n,e);
    return 0;
}